import json

from datetime import datetime
from typing import Optional
from dataclasses import asdict

from django.core import serializers
from django.db.models.query import QuerySet
from django.utils.timezone import make_aware
from django.utils import timezone
from django_celery_beat.models import ClockedSchedule, \
    IntervalSchedule, PeriodicTask

from send_mail.models import Client
from send_mail.services.dataclass import SendMessageData


class PeriodicTaskCreator():
    """
    Создает PeriodicTask.
    Если на вход получает коллекцию моделей Client, 
    то формирует PeriodicTask для repeat_mailing task.
    Если одного Client, то PeriodicTask для send_message_task.
    """
    def __init__(self, 
            start_timedata: str | datetime,
            stop_timedata: str | datetime,
            mailing_id: int,
            clients: Optional[QuerySet[Client]] = None,
            send_data: Optional[SendMessageData] = None) -> None:

        if isinstance(start_timedata, datetime):
            self.start = start_timedata
        else:
            self.start = make_aware(
                datetime.strptime(start_timedata, '%Y-%m-%d %H:%M'))
        if isinstance(stop_timedata, datetime):
            self.stop = stop_timedata
        else:
            self.stop = make_aware(
                datetime.strptime(stop_timedata, '%Y-%m-%d %H:%M'))
        self.mailing_id = mailing_id
        self.send_data = send_data
        self.clients = clients
        self.interval = IntervalSchedule.objects.get_or_create(every=10, period='seconds')

    def create(self):
        if self.clients:
            json_query = serializers.serialize('json', self.clients)
            PeriodicTask.objects.create(
                name=f'periodic_repeat_mailing {self.mailing_id}',
                task='repeat_mailing',
                clocked=ClockedSchedule.objects.create(clocked_time=self.start.isoformat()),
                one_off=True, 
                expires=self.stop.isoformat(),
                args=json_query,
                kwargs='{"mailing_id": "%s"}' % self.mailing_id,
                start_time=timezone.now(),
            )
        elif self.send_data:
            PeriodicTask.objects.create(
                name=f'periodic_send_message {self.send_data.message_id}',
                task='send_message_task',
                interval=self.interval[0],
                expires=self.stop.isoformat(),
                args =f'[{json.dumps(asdict(self.send_data))}]',
                start_time=timezone.now(),
            )
