from dataclasses import dataclass

from rest_framework import serializers
        

@dataclass
class SendMessageData():
    """
    Модель данных для send_mail_task.
    """
    message_id: int
    client_phone: int
    text: str

    def __getitem__(self, item):
        return getattr(self, item)

    def __post_init__(self):
        try:
            int(self.message_id)
            int(self.client_phone)
            str(self.text)
        except ValueError:
            raise serializers.ValidationError(
                'wrong data entry to SendMessageData')

