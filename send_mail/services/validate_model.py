import logging

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from send_mail.services.text_messages import CODE_ERROR, \
    PHONE_LEN_ERR, PHONE_FIRST_DIGITAL_ERR


logger = logging.getLogger(__name__)


def validate_phone(value: int) -> None:
    """
    Проверяет корректность номера клиента.
    Критерии: 
    - Всего цифр 11
    - Первая цифра -> 7
    """
    if len(str(value)) != 11:
        raise ValidationError(
            _(PHONE_LEN_ERR),
            params={"value": value},
        )
    elif str(value)[0] != '7':
        raise ValidationError(
            _(PHONE_FIRST_DIGITAL_ERR),
            params={"value": value},
        )


def validate_code_tag(code_tag: str) -> None:
    """
    Проверяет корректность поля filter_clients.
    Код должен состоять из цифр.
    """
    code, tag = code_tag.split()
    try:
        code = int(code)
    except ValueError as e:
        logging.warning('wrong client code', e)
        raise ValidationError(
            _(CODE_ERROR),
            params={"value": code_tag},
        )

