import logging
from datetime import datetime

from django.db.models.query import QuerySet
from django.utils import timezone
from rest_framework import serializers

from send_mail.models import Client
from send_mail.services.text_messages import DATETIME_SEQ_ERROR, \
    CODE_TAG_NO_HITS, DATETIME_PAST_ERROR


logger = logging.getLogger(__name__)


def validate_mailings_timedata(
        start_timedata: datetime,
        stop_timedata: datetime) -> None:
    """
    Проверяет даты.
    Даты не должны быть прошедшими,
    дата запуска должна быть до даты окончания.
    """
    tz = timezone.get_current_timezone()
    start_date = start_timedata.replace(tzinfo=tz)
    stop_date = stop_timedata.replace(tzinfo=tz)

    if start_date < timezone.now() or stop_date < timezone.now():
        raise serializers.ValidationError(DATETIME_PAST_ERROR)
    elif start_date > stop_date:
        raise serializers.ValidationError(DATETIME_SEQ_ERROR)


def code_tag_filter(code: int, tag: str) -> QuerySet[Client]:
    """
    По code и tag фильтрует клиентов, 
    Возвращает QuerySet моделей, 
    Если совпадений нет, выводит сообщение об ошибке.
    """
    filter_clients = Client.objects.filter(
        mobile_code=code, tag=tag)
    if filter_clients:
        return filter_clients
    else: 
        raise serializers.ValidationError(CODE_TAG_NO_HITS)


