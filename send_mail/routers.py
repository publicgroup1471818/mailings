from rest_framework import routers

from send_mail.views import MailingsView, ClientView, \
    MessagesView


mailings_router = routers.SimpleRouter()
mailings_router.register(r'', MailingsView)

client_router = routers.SimpleRouter()
client_router.register(r'', ClientView)


message_router = routers.SimpleRouter()
message_router.register(r'', MessagesView)
