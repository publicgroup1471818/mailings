from django.contrib import admin

from send_mail.models import Message, Client, Mailings


@admin.register(Mailings)
class MailingsAdmin(admin.ModelAdmin):
    list_display = (
        'date_time_start_send_mail', 
        'date_time_stop_send_mail',
        'text_for_client',
        'filter_clients',
    )
    list_filter = ('date_time_start_send_mail', )


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = (
        'date_time_mailing', 
        'status_mailing',
        'current_mailing',
        'current_client',
    )
    list_filter = ('date_time_mailing', )


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = (
        'phone', 
        'mobile_code',
        'tag',
        'time_zone',
    )
