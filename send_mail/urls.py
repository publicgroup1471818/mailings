from django.urls import path, include

from send_mail.routers import mailings_router, \
    client_router, message_router


urlpatterns = [
    path('mailings/', include(mailings_router.urls)),
    path('client/', include(client_router.urls)),
    path('messages/', include(message_router.urls)),
]
