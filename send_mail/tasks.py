import requests
        
from celery import shared_task
from celery.utils.log import get_task_logger

from django_celery_beat.models import PeriodicTask
from django_celery_beat.utils import timezone

from send_mail.models import Mailings, Message
from send_mail.services.dataclass import SendMessageData
from send_mail.services.mailing import PeriodicTaskCreator
from config.settings import TOKEN


logger = get_task_logger(__name__)


@shared_task(name='repeat_mailing') 
def handles_cliens_query(*args, **kwargs) -> None:
    """
    Обрабатывает список клиентов, 
    Создает PeriodicTask для каждого.
    """ 
    cur_mailing = Mailings.objects.get(pk=int(kwargs['mailing_id'])) 

    start = cur_mailing.date_time_start_send_mail
    stop = cur_mailing.date_time_stop_send_mail

    for cur_client in args:
        message = Message.objects.create(
            date_time_mailing = timezone.now(),
            current_mailing_id = int(kwargs['mailing_id']),
            current_client_id = cur_client['pk'],
        )
        message.save()
        send_mess_per_task = PeriodicTaskCreator(
            start_timedata = start,
            stop_timedata = stop,
            mailing_id = int(kwargs['mailing_id']),
            send_data = SendMessageData(
                message_id=message.pk,
                client_phone=int(cur_client['fields']['phone']),  
                text = cur_mailing.text_for_client,
            ) 
        )
        send_mess_per_task.create()


@shared_task(name='send_message_task')
def send_message_task(*args) -> None:
    """
    Собирает и отправляет сообщение.
    Возвращает ответ сервера.
    """
    obj = SendMessageData(
        message_id = args[0]['message_id'],
        client_phone = args[0]['client_phone'],
        text = args[0]['text'],
    )
    url = f"https://probe.fbrq.cloud/v1/send/{obj.message_id}"
    send_data = {
      "id": obj.message_id,
      "phone": obj.client_phone,
      "text": obj.text,
    }
    headers={
        "Authorization": f"Bearer {TOKEN}",
        "accept": "application/json",
        "Content-Type": "application/json"
    }
    resp = requests.post(url, json=send_data, headers=headers)

    if resp.status_code == 200:
        task = PeriodicTask.objects.get(
            name=f'periodic_send_message {obj.message_id}')
        task.enabled = False
        task.save()
        cur_message = Message.objects.get(pk=args[0]['message_id'])
        cur_message.status_mailing = 2
        cur_message.save()
        logger.info(f'Success message send. response -> {resp.text}')

