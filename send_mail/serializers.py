from rest_framework import serializers

from send_mail.models import Client, Mailings, Message


class MailigsSerializer(serializers.ModelSerializer):
    date_time_start_send_mail = serializers.DateTimeField(
        input_formats=[
            '%d.%m.%Y %H:%M',
            'iso-8601'
        ], required=False)
    date_time_stop_send_mail = serializers.DateTimeField(
        input_formats=[
            '%d.%m.%Y %H:%M',
            'iso-8601'
        ], required=False)
    
    class Meta:
        model = Mailings
        fields = '__all__'


class ClientsSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Client
        fields = '__all__'


class MessagesSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Message
        fields = '__all__'


class MailigsListSerializer(serializers.ModelSerializer):
    messages = MessagesSerializer(source='message_set', many=True)
    
    class Meta:
        model = Mailings
        depth = 1
        fields = '__all__'

