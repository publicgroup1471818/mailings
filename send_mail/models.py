from django.db import models

from send_mail.services.validate_model import validate_phone, \
    validate_code_tag


class Mailings(models.Model):
    
    class Meta:
        db_table = 'Mailings'
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'
        ordering = ['pk']

    date_time_start_send_mail = models.DateTimeField(
        verbose_name='Дата и время запуска рассылки.',
    )
    date_time_stop_send_mail = models.DateTimeField(
        verbose_name='Дата и время окончания рассылки.',
    )
    text_for_client = models.TextField(
        verbose_name='Текст сообщения для доставки клиенту.',
    )
    filter_clients = models.CharField(
        max_length=50, 
        null=True,
        verbose_name='Код и таг клиента, через пробел.',
        validators=[validate_code_tag],
    )
    def __str__(self):
        return f'Рассылка №{self.pk}'


class Client(models.Model):

    class Meta:
        db_table = 'Client'
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
        ordering = ['pk']

    phone = models.BigIntegerField(
        verbose_name='Номер телефона',
        unique=True,
        validators=[validate_phone],
    )
    mobile_code = models.PositiveSmallIntegerField(   
        verbose_name='Код мобильного оператора', 
    )
    tag = models.CharField(
        max_length = 25,
        verbose_name='тег (произвольная метка)',
    )
    time_zone = models.CharField(
        max_length=100,
        verbose_name='часовой пояс'
    )
    def __str__(self):
        return f'Клиент №{self.pk}'


class Message(models.Model):

    class Meta:
        db_table = 'Message'
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ['pk']

    STATUS = (
        (1, 'В процессе отправки.'),
        (2, 'Доставлен.'),
    )
    date_time_mailing = models.DateTimeField(
        auto_now=True, 
        verbose_name='Дата и время создания (отправки).',
    )
    status_mailing = models.PositiveSmallIntegerField(
        choices=STATUS, default=1, verbose_name='Статус',
    )
    current_mailing = models.ForeignKey(
        Mailings, on_delete=models.PROTECT,
        verbose_name='id рассылки',
    )
    current_client = models.ForeignKey(
        Client, on_delete=models.PROTECT,
        verbose_name='id клиента',
    )
    def __str__(self):
        return f'Сообщение №{self.pk}'


