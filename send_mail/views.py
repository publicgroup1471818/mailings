import logging 

from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action

from send_mail.models import Client, Mailings, Message
from send_mail.serializers import ClientsSerializer, \
    MailigsListSerializer, MailigsSerializer, MessagesSerializer
from send_mail.services.mailing import PeriodicTaskCreator
from send_mail.services.validate import code_tag_filter, \
    validate_mailings_timedata


logger = logging.getLogger(__name__)


class ClientView(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientsSerializer
    http_method_names = ['get', 'post', 'delete', 'patch', 'head']
    
    def create(self, request, *args, **kwargs) -> Response:
        serializer = self.get_serializer(data=request.data) 
        serializer.is_valid(raise_exception=True)
        serializer.save()
        logger.info('create new client')

        return Response(serializer.data, status=status.HTTP_200_OK)


class MailingsView(viewsets.ModelViewSet):
    queryset = Mailings.objects.all()
    serializer_class = MailigsSerializer
    http_method_names = ['get', 'post', 'delete', 'patch', 'head']

    def list(self, request, *args, **kwargs) -> Response:
        queryset = Mailings.objects.all()
        serializer = MailigsListSerializer(
            queryset.prefetch_related(
                'message_set').all().order_by(
                'message__status_mailing'), many=True)
        return Response(serializer.data)
 
    @action(detail=True, methods=['get'])
    def current_statistic(self, request, pk=None) -> Response:  
        """Статистика конкретной рассылки."""
        cur_mailing = Mailings.objects.get(pk=pk)
        serializer_mailing = MailigsListSerializer(cur_mailing)
        return Response(serializer_mailing.data, status=status.HTTP_200_OK)
    
    def create(self, request, *args, **kwargs) -> Response:
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validate_mailings_timedata(
            serializer.validated_data['date_time_start_send_mail'],
            serializer.validated_data['date_time_stop_send_mail']
        )
        code, tag = serializer.validated_data['filter_clients'].split()

        if client_id_book := code_tag_filter(code, tag):
            self.perform_create(serializer)
            mailing_per_task = PeriodicTaskCreator(
                start_timedata = serializer.data[
                    'date_time_start_send_mail'
                ],
                stop_timedata = serializer.data[
                    'date_time_stop_send_mail'
                ],
                mailing_id = serializer.data['id'],
                clients = client_id_book,
            )
            mailing_per_task.create()
            logger.info('create new mailing')

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class MessagesView(viewsets.ModelViewSet):
    queryset = Message.objects.all().select_related(
        'current_mailing', 'current_client'
    )
    serializer_class = MessagesSerializer
    http_method_names = ['get']
    
